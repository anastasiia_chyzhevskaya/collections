package src;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> collection = new ArrayList<>();
        final Random random = new Random();
        for (int i = 0; i < 10; i++) {
            collection.add(random.nextInt());
        }
        HashSet<Integer> collection2 = new HashSet<>();
        collection2.addAll(collection);
        System.out.println(collection2);
        collection.removeAll(collection);

        collection.addAll(collection2);
        Collections.sort(collection);
        System.out.println(collection);
        int min = Collections.min(collection);
        int max = Collections.max(collection);
        System.out.println("Max: " + max);
        System.out.println("Min: " + min);
        int sum = 0;
        for (int num : collection) {
            sum = sum + num;
        }
        System.out.println("Sum: " + sum);
        collection2.removeAll(collection2);
        for (int value : collection) {
            if (value >= 0) {
                collection2.add(value);
            }
        }
        System.out.println("Collection without negative numbers: " + collection2);
        for (int i = 0; i < 5; i++) {
            if (collection.get(i) % 2 != 0) {
                collection.remove(i);
            }
        }
        System.out.println("even numbers: " + collection);
        if (collection.contains(7)) {
            System.out.println("Collection contains 7!");
        } else {
            System.out.println("Error: element 7 expected");
        }
    }
}


